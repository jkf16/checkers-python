# checkers-python

This is the recreation of the game "checkers" in python, in a CLI.
The objective is to recreate some of the main rules of the game, like.
- You can only move one cell per movement
- You can eat one or make a chain eating if you can do that.
- You can crown your piece once it has reached the first line of the oponent field
- That crowned piece can move more than one cell indicated.
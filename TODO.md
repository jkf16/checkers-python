# v1.0
Fully functional game with all the main aspects we want.

These aspects are:
- The game shows a good board ressembling to an actual board with white/black patterns.
- Two players, IA not necessary in the game for now.
- Every main movement of this game.
- For every move you can see graphically what moves you can actually make and what of those you can't perform.
- The game has an end, basically you can either win or lose.
- The keyboard input is prefferably to be with the arrow keys but it can also be with coordinates (using number from 1-9 and letters, from A-I)

# v0.1
- [ ] Game board printed succesfully
- [ ] Show actual game turns (alternating between player one and two)

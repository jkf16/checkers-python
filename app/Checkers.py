class Checkers:
    def __init__(self):
        '''
        Class constructor for the whole game
        '''
        self.__game_end = False

        self.__board = []
        for board in range(0,8):
            linestring = ''

            if(board % 2 == 0):
                num = 0
            else:
                num = 1
            for line in range(0,8):
                
                if(line % 2 == num): 
                    linestring += u'\u2178'
                else:
                    linestring += ' '
            self.__board.append(linestring)


    def __print_game_board(self):
        '''
        Method that prints the game board
        '''
        
        for line in self.__board:
            print(line)

    def play(self):
        '''
        This is the method that handles the game start
        '''
        while(not self.__game_end):
            self.__print_game_board()

            self.__game_end = True

            #TODO declare self.__ask_user_input()

            #TODO declare self.__game_status = self.__process()